import pytest
import sys


if __name__ == '__main__':
    print('Tests are running. Please, wait.\nResults will be published to results.txt')
    sys.stdout = open('results.txt', 'w')
    pytest.main(args=['-v',
                      '--alluredir=test_results'])
    sys.stdout.close()
    sys.stdout = sys.__stdout__
